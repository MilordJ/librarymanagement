/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.model;

import com.neuron.cm.domain.Carrier;
import com.neuron.cm.domain.EntityLibraryScientificAuthor;
import com.neuron.cm.domain.EntityLibraryScientificResearch;
import com.neuron.cm.domain.SessionManager;
import com.neuron.cm.util.EntityManager;
import com.neuron.cm.view.EntityLibraryV_ScAuthor_v4;
import com.neuron.cm.view.EntityLibraryV_ScientificResearchs_v4;
import java.util.Arrays;

/**
 *
 * @author Lito
 */
public class ScientificResearchModel {
    
     public void insertNewScientificResearch() throws Exception {
         String tableName = "authors";
         Carrier c = SessionManager.sessionCarrier();
            EntityLibraryScientificResearch ent = new EntityLibraryScientificResearch();
            EntityManager.doInsert(ent);
            String scientificId = ent.getId();
            
            int rowCount = c.getTableRowCount(tableName);
            for(int i = 0; i < rowCount; i++) {
                EntityLibraryScientificAuthor ent2 = new EntityLibraryScientificAuthor();
                Carrier carrier = new Carrier();
                carrier.setValue("authorId", c.getValue(tableName, i, "authorId"));
                carrier.setValue("scientificId", scientificId);
                EntityManager.doInsertSpec(ent2, carrier);
            }
    }
    public void updateScientificResearch() throws Exception {
         String tableName = "authors";
         Carrier c = SessionManager.sessionCarrier();
            EntityLibraryScientificResearch ent = new EntityLibraryScientificResearch();
            EntityManager.doUpdate(ent);
            String scientificId = c.getValue("id").toString();
            EntityLibraryScientificAuthor ent2 = new EntityLibraryScientificAuthor();
            Carrier carrier = new Carrier();
            carrier.setValue("scientificId", scientificId);
            EntityManager.doDeleteByOtherParams(ent2, carrier, Arrays.asList("scientificId"));
            int rowCount = c.getTableRowCount(tableName);
            for(int i = 0; i < rowCount; i++) {
                ent2 = new EntityLibraryScientificAuthor();
                carrier = new Carrier();
                carrier.setValue("authorId", c.getValue(tableName, i, "authorId"));
                carrier.setValue("scientificId", scientificId);
                EntityManager.doInsertSpec(ent2, carrier);
            }
    }
        
    public void deleteScientificResearch() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
           EntityLibraryScientificResearch ent = new EntityLibraryScientificResearch();
           EntityManager.doDelete(ent);
           String scientificId = c.getValue("id").toString();
           EntityLibraryScientificAuthor ent2 = new EntityLibraryScientificAuthor();
           Carrier carrier = new Carrier();
           carrier.setValue("scientificId", scientificId);
            EntityManager.doDeleteByOtherParams(ent2, carrier, Arrays.asList("scientificId"));
           
    }
    
    public void getScientificResearchList() throws Exception {
        EntityLibraryV_ScientificResearchs_v4 ent = new EntityLibraryV_ScientificResearchs_v4();
        EntityManager.doSelect(ent);
    }
    
    public void getScientificResearchAuthors() throws Exception {
        EntityLibraryV_ScAuthor_v4 ent = new EntityLibraryV_ScAuthor_v4();
        EntityManager.doSelect(ent);
    }
    

}

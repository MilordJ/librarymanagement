/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.model;

import com.neuron.cm.domain.Carrier;
import com.neuron.cm.domain.EntityLibraryResources;
import com.neuron.cm.domain.EntityLibraryResourcesAuthor;
import com.neuron.cm.domain.EntityLibraryResourcesCompiler;
import com.neuron.cm.domain.EntityLibraryResourcesEdition;
import com.neuron.cm.domain.SessionManager;
import com.neuron.cm.util.EntityManager;
import com.neuron.cm.view.EntityLibraryV_ResourcesAuthor_v4;
import com.neuron.cm.view.EntityLibraryV_ResourcesCompiler_v4;
import com.neuron.cm.view.EntityLibraryV_ResourcesEdition_v4;
import com.neuron.cm.view.EntityLibraryV_Resources_v4;
import java.util.Arrays;

/**
 *
 * @author Lito
 */
public class ResourcesModel {
    
     public void insertNewResources() throws Exception {
        String tableNameAuthors = "authors";
        String tableNameEditions = "editions";
        String tableNameCompilers = "compilers";
        Carrier c = SessionManager.sessionCarrier();
        EntityLibraryResources ent = new EntityLibraryResources();
        EntityManager.doInsert(ent);
        String resourceId = ent.getId();
        int rowCountAuthor = c.getTableRowCount(tableNameAuthors);
        int rowCountEdition = c.getTableRowCount(tableNameEditions);
        int rowCountCompiler = c.getTableRowCount(tableNameCompilers);
            
        for(int i = 0; i < rowCountAuthor; i++) {
            EntityLibraryResourcesAuthor ent2 = new EntityLibraryResourcesAuthor();
            Carrier carrier = new Carrier();
            carrier.setValue("authorId", c.getValue(tableNameAuthors, i, "authorId"));
            carrier.setValue("resourcesId", resourceId);
            EntityManager.doInsertSpec(ent2, carrier);
        }
        for (int i = 0; i < rowCountEdition; i++) {
            EntityLibraryResourcesEdition ent3 = new EntityLibraryResourcesEdition();
            Carrier carrier = new Carrier();
            carrier.setValue("resourcesId", resourceId);
            carrier.setValue("pageCount", c.getValue(tableNameEditions, i, "pageCount"));
            carrier.setValue("publishDate", c.getValue(tableNameEditions, i, "publishDate"));
            carrier.setValue("rowSerial", c.getValue(tableNameEditions, i, "rowSerial"));
            carrier.setValue("price", c.getValue(tableNameEditions, i, "price").toString());
            EntityManager.doInsertSpec(ent3, carrier);
        }
        for (int i = 0; i < rowCountCompiler; i++) {
            EntityLibraryResourcesCompiler ent4 = new EntityLibraryResourcesCompiler();
            Carrier carrier = new Carrier();
            carrier.setValue("resourcesId", resourceId);
            carrier.setValue("compilerType", c.getValue(tableNameCompilers, i, "compilerType"));
            carrier.setValue("compilerName", c.getValue(tableNameCompilers, i, "compilerName"));
            EntityManager.doInsertSpec(ent4, carrier);
        }
    }

     
    public void updateResources() throws Exception {
         Carrier c = SessionManager.sessionCarrier();
            EntityLibraryResources ent = new EntityLibraryResources();
            EntityManager.doUpdate(ent);
            String resourceId = c.getValue("id").toString();
            
            EntityLibraryResourcesAuthor ent2 = new EntityLibraryResourcesAuthor();
            EntityLibraryResourcesCompiler ent3 = new EntityLibraryResourcesCompiler();
            EntityLibraryResourcesEdition ent4 = new EntityLibraryResourcesEdition();
            
            Carrier carrier = new Carrier();
            carrier.setValue("resourcesId", resourceId);
            EntityManager.doDeleteByOtherParams(ent2, carrier, Arrays.asList("resourcesId"));
            carrier = new Carrier();
            carrier.setValue("resourcesId", resourceId);
            EntityManager.doDeleteByOtherParams(ent3, carrier, Arrays.asList("resourcesId"));
            carrier = new Carrier();
            carrier.setValue("resourcesId", resourceId);
            EntityManager.doDeleteByOtherParams(ent4, carrier, Arrays.asList("resourcesId"));
            
            String tableNameAuthors = "authors";
            String tableNameEditions = "editions";
            String tableNameCompilers = "compilers";
            int rowCountAuthor = c.getTableRowCount(tableNameAuthors);
            int rowCountEdition = c.getTableRowCount(tableNameEditions);
            int rowCountCompiler = c.getTableRowCount(tableNameCompilers);
            
            for(int i = 0; i < rowCountAuthor; i++) {
                ent2 = new EntityLibraryResourcesAuthor();
                Carrier c1 = new Carrier();
                c1.setValue("authorId", c.getValue(tableNameAuthors, i, "authorId"));
                c1.setValue("resourcesId", resourceId);
                EntityManager.doInsertSpec(ent2, c1);
            }
            for (int i = 0; i < rowCountCompiler; i++) {
                ent3 = new EntityLibraryResourcesCompiler();
                Carrier c1 = new Carrier();
                c1.setValue("resourcesId", resourceId);
                c1.setValue("compilerType", c.getValue(tableNameCompilers, i, "compilerType"));
                c1.setValue("compilerName", c.getValue(tableNameCompilers, i, "compilerName"));
                EntityManager.doInsertSpec(ent3, c1);
            }            
            for (int i = 0; i < rowCountEdition; i++) {
                ent4 = new EntityLibraryResourcesEdition();
                Carrier c1 = new Carrier();
                c1.setValue("resourcesId", resourceId);
                c1.setValue("pageCount", c.getValue(tableNameEditions, i, "pageCount"));
                c1.setValue("publishDate", c.getValue(tableNameEditions, i, "publishDate"));
                c1.setValue("rowSerial", c.getValue(tableNameEditions, i, "rowSerial"));
                c1.setValue("price", c.getValue(tableNameEditions, i, "price"));
                EntityManager.doInsertSpec(ent4, c1);
         }
    }
        
    public void deleteResources() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
           EntityLibraryResources ent = new EntityLibraryResources();
           EntityManager.doDelete(ent);
           String resourcesId = c.getValue("id").toString();
           EntityLibraryResourcesAuthor ent2 = new EntityLibraryResourcesAuthor();
           EntityLibraryResourcesCompiler ent3 = new EntityLibraryResourcesCompiler();
           EntityLibraryResourcesEdition ent4 = new EntityLibraryResourcesEdition();
           Carrier carrier = new Carrier();
           carrier.setValue("resourcesId", resourcesId);
           EntityManager.doDeleteByOtherParams(ent2, carrier, Arrays.asList("resourcesId"));
           carrier = new Carrier();
           carrier.setValue("resourcesId", resourcesId);
           EntityManager.doDeleteByOtherParams(ent3, carrier, Arrays.asList("resourcesId"));
           carrier = new Carrier();
           carrier.setValue("resourcesId", resourcesId);
           EntityManager.doDeleteByOtherParams(ent4, carrier, Arrays.asList("resourcesId"));
           
    }
    
    public void getResourcesList() throws Exception {
        EntityLibraryV_Resources_v4 ent = new EntityLibraryV_Resources_v4();
        EntityManager.doSelect(ent);
    }
    
    public void getResourcesListCommon() throws Exception {
        EntityLibraryV_Resources_v4 ent = new EntityLibraryV_Resources_v4();
        EntityManager.doSelect(ent);
    }
    
    public void getResourcesAuthor() throws Exception {
        EntityLibraryV_ResourcesAuthor_v4 ent = new EntityLibraryV_ResourcesAuthor_v4();
        EntityManager.doSelect(ent);
    }
    
    public void getResourcesAuthorCommon() throws Exception {
        EntityLibraryV_ResourcesAuthor_v4 ent = new EntityLibraryV_ResourcesAuthor_v4();
        EntityManager.doSelect(ent);
    }
    public void getResourcesCompiler() throws Exception {
        EntityLibraryV_ResourcesCompiler_v4 ent = new EntityLibraryV_ResourcesCompiler_v4();
        EntityManager.doSelect(ent);
    }
    public void getResourcesEdition() throws Exception {
        EntityLibraryV_ResourcesEdition_v4 ent = new EntityLibraryV_ResourcesEdition_v4();
        EntityManager.doSelect(ent);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.model;

import com.neuron.cm.domain.EntityLibraryLibDicAuthors;
import com.neuron.cm.util.EntityManager;
import com.neuron.cm.view.EntityLibraryV_Authors_v4;

/**
 *
 * @author Lito
 */
public class AuthorModel {
    public void insertNewAuthor() throws Exception {
         EntityLibraryLibDicAuthors ent = new EntityLibraryLibDicAuthors();
         EntityManager.doInsert(ent);
    }
    public void updateAuthor() throws Exception {
         EntityLibraryLibDicAuthors ent = new EntityLibraryLibDicAuthors();
         EntityManager.doUpdate(ent);
    }
        
    public void deleteAuthor() throws Exception {
        EntityLibraryLibDicAuthors ent = new EntityLibraryLibDicAuthors();
        EntityManager.doDelete(ent);
    }
    
    public void getAuthorList() throws Exception {
        EntityLibraryV_Authors_v4 ent = new EntityLibraryV_Authors_v4();
        EntityManager.doSelect(ent);
    }
}

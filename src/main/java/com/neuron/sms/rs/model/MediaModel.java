/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.model;

import com.neuron.cm.domain.EntityLibraryMedia;
import com.neuron.cm.util.EntityManager;
import com.neuron.cm.view.EntityLibraryV_Media_v4;

/**
 *
 * @author Lito
 */
public class MediaModel {
    
     public void insertNewMedia() throws Exception {
        EntityLibraryMedia ent = new EntityLibraryMedia();
        EntityManager.doInsert(ent);
    }
     
    public void updateMedia() throws Exception {
        EntityLibraryMedia ent = new EntityLibraryMedia();
        EntityManager.doUpdate(ent);
    }
   
    public void deleteMedia() throws Exception {
        EntityLibraryMedia ent = new EntityLibraryMedia();
        EntityManager.doDelete(ent);
    }
    
    public void getMediaList() throws Exception {
        EntityLibraryV_Media_v4 ent = new EntityLibraryV_Media_v4();
        EntityManager.doSelect(ent);
    }
    
}

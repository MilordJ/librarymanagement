/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.model;

import com.neuron.cm.domain.Carrier;
import com.neuron.cm.domain.EntityLibraryLibraryRequests;
import com.neuron.cm.domain.SessionManager;
import com.neuron.cm.enums.DictionaryType;
import com.neuron.cm.util.EntityManager;
import com.neuron.cm.view.EntityLibraryV_LibraryRequests_v4;

/**
 *
 * @author Ünalll
 */
public class RequestModel {
    
    // These are for ADMINSTRATIVE STUFF
    
    public void insertNewLibraryRequests() throws Exception {
        EntityLibraryLibraryRequests requests = new EntityLibraryLibraryRequests();
        EntityManager.doInsert(requests);
    }    
    
    public void updateLibraryRequests() throws Exception {
        EntityLibraryLibraryRequests requests = new EntityLibraryLibraryRequests();
        EntityManager.doUpdate(requests);
    }
    
    public void deleteLibraryRequests() throws Exception {
        EntityLibraryLibraryRequests requests = new EntityLibraryLibraryRequests();
        EntityManager.doDelete(requests);
    }
    
    public void getLibraryRequestsList() throws Exception {
        EntityLibraryV_LibraryRequests_v4 requests_v4 = new EntityLibraryV_LibraryRequests_v4();
        EntityManager.doSelect(requests_v4);
    }    
    
    // These are for COMMUNITY
    
    public void insertNewLibraryRequestsCommon() throws Exception {
        EntityLibraryLibraryRequests requests = new EntityLibraryLibraryRequests();
        EntityManager.doInsert(requests);
    }
    
    public void updateLibraryRequestsCommon() throws Exception {
        EntityLibraryLibraryRequests requests = new EntityLibraryLibraryRequests();
        EntityManager.doUpdate(requests);
    }
//    
//    public void deleteLibraryRequestsCommon() throws Exception {
//        EntityLibraryLibraryRequests requests = new EntityLibraryLibraryRequests();
//        EntityManager.doDelete(requests);
//    }
    
    public void getLibraryRequestsListCommon() throws Exception {
        EntityLibraryV_LibraryRequests_v4 ent = new EntityLibraryV_LibraryRequests_v4();
        EntityManager.doSelect(ent);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.validation;

import com.neuron.cm.domain.Carrier;
import com.neuron.cm.domain.EntityLibraryScientificResearch;
import com.neuron.cm.domain.SessionManager;
import com.neuron.cm.util.EntityManager;
import com.neuron.cm.view.EntityLibraryV_ScAuthor_v4;
import com.neuron.cm.view.EntityLibraryV_ScientificResearchs_v4;
import com.neuron.sms.rs.enums.RequiredConstants;

/**
 *
 * @author Lito
 */
public class ScientificResearchValidator {
    
    Class domainLibraryScientificResearch = EntityLibraryScientificResearch.class;
    
    public void insertNewScientificResearch() throws Exception {
        String tableNameAuthors = "authors";
        Carrier c = SessionManager.sessionCarrier();
        c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertScientificResearch());
        c.checkParamsTBL(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertResourcesAuthor(), tableNameAuthors);
        ChangeFieldFormatValidator.changeDateFormat(domainLibraryScientificResearch);        
        int rowCountAuthor = c.getTableRowCount(tableNameAuthors);
        if (rowCountAuthor == 0) {
            c.addError("invalidAuthor", "Author isn't choosen!");
            return;
        }
        String uniId = SessionManager.getCurrentUserUni();
        c.setValue("orgId", uniId);
    }
    public void updateScientificResearch() throws Exception {
         Carrier c = SessionManager.sessionCarrier();
         c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getUpdateScientificResearch());
         ChangeFieldFormatValidator.changeDateFormat(domainLibraryScientificResearch);
         String id = c.getValue("id").toString();
         EntityLibraryV_ScientificResearchs_v4 ent = new EntityLibraryV_ScientificResearchs_v4();
         Carrier carrier = new Carrier();
         carrier.setValue("id", id);
         String orgId = EntityManager.getColumnValueByColumnName(ent, "orgId", carrier);
         String uniId = SessionManager.getCurrentUserUni();
         if(orgId == null || orgId.trim().isEmpty() || !orgId.equals(uniId)){
             c.addError("Id", "Invalid research id!!!");
             return;
         }
    }
        
    public void deleteScientificResearch() throws Exception {
       Carrier c = SessionManager.sessionCarrier();
       c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getDeleteScientificResearch());
       String id = c.getValue("id").toString();
       EntityLibraryV_ScientificResearchs_v4 ent = new EntityLibraryV_ScientificResearchs_v4();
       Carrier carrier = new Carrier();
       carrier.setValue("id", id);
       String orgId = EntityManager.getColumnValueByColumnName(ent, "orgId", carrier);
       String uniId = SessionManager.getCurrentUserUni();
       if(orgId == null || orgId.trim().isEmpty() || !orgId.equals(uniId)){
           c.addError("Id", "Privilege reqired!!!");
       }
    }
    
    public void getScientificResearchList() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String uniId = SessionManager.getCurrentUserUni();
        if(!SessionManager.getCurrentUserType().equals("SYSADMIN") && !SessionManager.getCurrentUserType().equals("ADMIN")){
            c.setValue("orgId", uniId);
        }
    }
    
    public void getScientificResearchAuthors() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String scientificId = c.getValue("scientificId").toString();
        if(SessionManager.getCurrentUserType().equals("STUDENT"))
            c.addError("userType", "Invalid user type");
        if(scientificId == null || scientificId.trim().isEmpty()){
            c.addError("Invalid scientific Id", scientificId);
            return;
        }
        EntityLibraryV_ScientificResearchs_v4 researchs_v4 = new EntityLibraryV_ScientificResearchs_v4();
        Carrier c1 = new Carrier();
        c1.setValue("id", scientificId);
        int rowCount = EntityManager.getRowCount(researchs_v4, c1);            
        if(rowCount==0){
            c.addError("Not found", "Research not found!");
            return;
        }
        EntityLibraryV_ScAuthor_v4 ent = new EntityLibraryV_ScAuthor_v4();
        Carrier carrier = new Carrier();
        carrier.setValue("scientificId", scientificId);
        int rowCountAuthor = EntityManager.getRowCount(ent, carrier);
        if(rowCountAuthor==0){
            c.addError("Not found", "Author not found!");
            return;
        }
    }
       
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.validation;

import com.neuron.cm.domain.Carrier;
import com.neuron.cm.domain.EntityLibraryResourcesEdition;
import com.neuron.cm.domain.SessionManager;
import com.neuron.cm.util.EntityManager;
import com.neuron.cm.view.EntityLibraryV_ResourcesAuthor_v4;
import com.neuron.cm.view.EntityLibraryV_ResourcesCompiler_v4;
import com.neuron.cm.view.EntityLibraryV_ResourcesEdition_v4;
import com.neuron.cm.view.EntityLibraryV_Resources_v4;
import com.neuron.sms.rs.enums.RequiredConstants;

/**
 *
 * @author Lito
 */
public class ResourcesValidator {

    Class domainResourcesEdition = EntityLibraryResourcesEdition.class;

    public void insertNewResources() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String uniId = SessionManager.getCurrentUserUni();
//        if (!SessionManager.getCurrentUserType().equals("SYSADMIN") && !SessionManager.getCurrentUserType().equals("ADMIN")) {
            c.setValue("orgId", uniId);
//        }
        if (SessionManager.getCurrentUserType().equals("STUDENT")) {
            c.addError("userType", "Invalid user type");
        }
        String tableNameAuthors = "authors";
        String tableNameEdition = "editions";
        String tableNameCompiler = "compilers";
        c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertResources());
        c.checkParamsTBL(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertResourcesAuthor(), tableNameAuthors);
        c.checkParamsTBL(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertResourcesEdition(), tableNameEdition);
        c.checkParamsTBL(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertResourcesCompiler(), tableNameCompiler);

        int editionRowCount = c.getTableRowCount(tableNameEdition);
        if (editionRowCount == 0) {
            c.addError("invalidEdition", "Edition is empty!");
            return;
        } else {
            for (int i = 0; i < editionRowCount; i++) {
                String publishDate = "01/01/" + c.getValue(tableNameEdition, i, "publishDate").toString();
                c.setValue(tableNameEdition, i, "publishDate", publishDate);
            }
        }
        ChangeFieldFormatValidator.changeDateFormatInTable(domainResourcesEdition, tableNameEdition);
        int rowCountAuthor = c.getTableRowCount(tableNameAuthors);
        if (rowCountAuthor == 0) {
            c.addError("invalidAuthor", "Author isn't choosen!");
            return;
        }
    }
//    public void insertResourcesEdition() throws Exception {
//         Carrier c = SessionManager.sessionCarrier();
//         c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertResourcesEdition());
//         String resourceId = c.getValue("resourceId").toString();
//         String uniId = SessionManager.getCurrentUserUni();
//         EntityLibraryV_Resources_v4 ent = new EntityLibraryV_Resources_v4();
//         Carrier carrier = new Carrier();
//         carrier.setValue("id", resourceId);
//         carrier.setValue("orgId", uniId);
//         int count = EntityManager.getRowCount(ent, carrier);
//         if(count == 0) {
//             c.addError("resourceId", "INvalid resource id!!!");
//         }
//         c.setValue("orgId", uniId);
//    }
//    public void insertResourcesCompiler() throws Exception {
//         Carrier c = SessionManager.sessionCarrier();
//         c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertResourcesCompiler());
//         String resourceId = c.getValue("resourceId").toString();
//         String uniId = SessionManager.getCurrentUserUni();
//          EntityLibraryV_Resources_v4 ent = new EntityLibraryV_Resources_v4();
//         Carrier carrier = new Carrier();
//         carrier.setValue("id", resourceId);
//         carrier.setValue("orgId", uniId);
//         int count = EntityManager.getRowCount(ent, carrier);
//         if(count == 0) {
//             c.addError("resourceId", "INvalid resource id!!!");
//         }
//         c.setValue("orgId", uniId);
//    }

    public void updateResources() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getUpdateResources());
        String id = c.getValue("id").toString();
        EntityLibraryV_Resources_v4 ent = new EntityLibraryV_Resources_v4();
        Carrier carrier = new Carrier();
        carrier.setValue("id", id);
        int rowCount = EntityManager.getRowCount(ent, carrier);
        if(rowCount == 0){
            c.addError("notFound", "Resource not found!");
            return;
        }
        String uniId = SessionManager.getCurrentUserUni();
        if (!SessionManager.getCurrentUserType().equals("SYSADMIN") && !SessionManager.getCurrentUserType().equals("ADMIN")) {
            String orgId = EntityManager.getColumnValueByColumnName(ent, "orgId", carrier);
            if (orgId == null || orgId.trim().isEmpty() || !orgId.equals(uniId)) {
                c.addError("Id", "Invalid resource id!!!");
                return;
            }
        }
//        if (!SessionManager.getCurrentUserType().equals("SYSADMIN") && !SessionManager.getCurrentUserType().equals("ADMIN")) {
//            c.setValue("orgId", uniId);
//        }
        if (SessionManager.getCurrentUserType().equals("STUDENT")) {
            c.addError("userType", "Invalid user type");
        }
        String tableNameAuthors = "authors";
        String tableNameEdition = "editions";
        String tableNameCompiler = "compilers";
        c.checkParamsTBL(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertResourcesAuthor(), tableNameAuthors);
        c.checkParamsTBL(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertResourcesEdition(), tableNameEdition);
        c.checkParamsTBL(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertResourcesCompiler(), tableNameCompiler);

        int editionRowCount = c.getTableRowCount(tableNameEdition);
        if (editionRowCount == 0) {
            c.addError("invalidEdition", "Edition is empty!");
            return;
        } else {
            for (int i = 0; i < editionRowCount; i++) {
                String publishDate = "01/01/" + c.getValue(tableNameEdition, i, "publishDate").toString();
                c.setValue(tableNameEdition, i, "publishDate", publishDate);
            }
        }
        ChangeFieldFormatValidator.changeDateFormatInTable(domainResourcesEdition, tableNameEdition);
        int rowCountAuthor = c.getTableRowCount(tableNameAuthors);
        if (rowCountAuthor == 0) {
            c.addError("invalidAuthor", "Author isn't choosen!");
            return;
        }
        
        
    }

    public void deleteResources() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getDeleteResources());
        String id = c.getValue("id").toString();
        EntityLibraryV_Resources_v4 ent = new EntityLibraryV_Resources_v4();
        Carrier carrier = new Carrier();
        carrier.setValue("id", id);
        String uniId = SessionManager.getCurrentUserUni();
        if (!SessionManager.getCurrentUserType().equals("SYSADMIN") && !SessionManager.getCurrentUserType().equals("ADMIN")) {
            String orgId = EntityManager.getColumnValueByColumnName(ent, "orgId", carrier);
            if (orgId == null || orgId.trim().isEmpty() || !orgId.equals(uniId)) {
                c.addError("Id", "Invalid resource id!!!");
                return;
            }
        }
    }

    public void getResourcesList() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String uniId = SessionManager.getCurrentUserUni();
        if (!SessionManager.getCurrentUserType().equals("SYSADMIN") && !SessionManager.getCurrentUserType().equals("ADMIN")) {
            c.setValue("orgId", uniId);
        }
    }

    public void getResourcesListCommon() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String uniId = SessionManager.getCurrentUserUni();
        if (!SessionManager.getCurrentUserType().equals("SYSADMIN") && !SessionManager.getCurrentUserType().equals("ADMIN")) {
            c.setValue("orgId", uniId);
        }
    }

    public void getResourcesDetails() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String resourceId = c.getValue("id").toString();
        if(SessionManager.getCurrentUserType().equals("STUDENT"))
            c.addError("userType", "Invalid user type");
        if(resourceId == null || resourceId.trim().isEmpty()){
            c.addError("Invalid resource Id", resourceId);
            return;
        }
    }
    
    public void getResourcesAuthor() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String resourceId = c.getValue("resourcesId").toString();
        if(SessionManager.getCurrentUserType().equals("STUDENT"))
            c.addError("userType", "Invalid user type");
        if(resourceId == null || resourceId.trim().isEmpty()){
            c.addError("Invalid resource Id", resourceId);
            return;
        }        
        EntityLibraryV_ResourcesAuthor_v4 ent = new EntityLibraryV_ResourcesAuthor_v4();
        Carrier carrier = new Carrier();
        carrier.setValue("resourcesId", resourceId);
        int rowCount = EntityManager.getRowCount(ent, carrier);
        if(rowCount==0){
            c.addError("Not found", "Resource not found!");
            return;
        }
    }
    
    public void getResourcesAuthorCommon() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String resourceId = c.getValue("resourcesId").toString();
        if(SessionManager.getCurrentUserType().equals("STUDENT"))
            c.addError("userType", "Invalid user type");
        if(resourceId == null || resourceId.trim().isEmpty()){
            c.addError("Invalid resource Id", resourceId);
            return;
        }        
        EntityLibraryV_ResourcesAuthor_v4 ent = new EntityLibraryV_ResourcesAuthor_v4();
        Carrier carrier = new Carrier();
        carrier.setValue("resourcesId", resourceId);
        int rowCount = EntityManager.getRowCount(ent, carrier);
        if(rowCount==0){
            c.addError("Not found", "Resource not found!");
            return;
        }
    }
    
    public void getResourcesCompiler() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String resourceId = c.getValue("resourcesId").toString();
        if(SessionManager.getCurrentUserType().equals("STUDENT"))
            c.addError("userType", "Invalid user type");
        if(resourceId == null || resourceId.trim().isEmpty()){
            c.addError("Invalid resource Id", resourceId);
            return;
        }        
//        EntityLibraryV_ResourcesCompiler_v4 ent = new EntityLibraryV_ResourcesCompiler_v4();
//        Carrier carrier = new Carrier();
//        carrier.setValue("resourcesId", resourceId);
//        int rowCount = EntityManager.getRowCount(ent, carrier);
//        if(rowCount==0){
//            c.addError("Not found", "Resource not found!");
//            return;
//        }
    }
    public void getResourcesEdition() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String resourceId = c.getValue("resourcesId").toString();
        EntityLibraryV_ResourcesEdition_v4 ent = new EntityLibraryV_ResourcesEdition_v4();
        Carrier carrier = new Carrier();
        carrier.setValue("resourcesId", resourceId);
        int rowCount = EntityManager.getRowCount(ent, carrier);
        if(rowCount==0){
            c.addError("Not found", "Resource not found!");
            return;
        }
        if(SessionManager.getCurrentUserType().equals("STUDENT"))
            c.addError("userType", "Invalid user type");
        if(resourceId == null || resourceId.trim().isEmpty()){
            c.addError("Invalid resource Id", resourceId);
            return;
        }
    }

//    public void getResourcesCompilerList() throws Exception {
//        Carrier c = SessionManager.sessionCarrier();
//        String uniId = SessionManager.getCurrentUserUni();
//        if(!SessionManager.getCurrentUserType().equals("SYSADMIN") && !SessionManager.getCurrentUserType().equals("ADMIN"))
//            c.setValue("orgId", uniId);
//    }
//    public void getResourcesEditionList() throws Exception {
//        Carrier c = SessionManager.sessionCarrier();
//        String uniId = SessionManager.getCurrentUserUni();
//        if(!SessionManager.getCurrentUserType().equals("SYSADMIN") && !SessionManager.getCurrentUserType().equals("ADMIN"))
//            c.setValue("orgId", uniId);
//    }
}

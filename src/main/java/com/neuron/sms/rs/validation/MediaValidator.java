/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.validation;

import com.neuron.cm.domain.Carrier;
import com.neuron.cm.domain.SessionManager;
import com.neuron.cm.util.EntityManager;
import com.neuron.cm.view.EntityLibraryV_Media_v4;
import com.neuron.sms.rs.enums.RequiredConstants;

/**
 *
 * @author Lito
 */
public class MediaValidator {
    public void insertNewMedia() throws Exception {
         Carrier c = SessionManager.sessionCarrier();
         c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertMedia());
         String uniId = SessionManager.getCurrentUserUni();
         c.setValue("orgId", uniId);
    }
    public void updateMedia() throws Exception {
         Carrier c = SessionManager.sessionCarrier();
         c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getUpdateMedia());
//         String id = c.getValue("id").toString();
//         EntityLibraryV_Media_v4 ent = new EntityLibraryV_Media_v4();
//         Carrier carrier = new Carrier();
//         carrier.setValue("id", id);
//         String orgId = EntityManager.getColumnValueByColumnName(ent, "orgId", carrier);
//         String uniId = SessionManager.getCurrentUserUni();
//         if(orgId == null || orgId.trim().isEmpty() || !orgId.equals(uniId)) {
//             c.addError("Id", "Invalid media id!!!");
//             return;
//         }
    }
        
    public void deleteMedia() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
         c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getDeleteMedia());
//         String id = c.getValue("id").toString();
//         EntityLibraryV_Media_v4 ent = new EntityLibraryV_Media_v4();
//         Carrier carrier = new Carrier();
//         carrier.setValue("id", id);
//         String orgId = EntityManager.getColumnValueByColumnName(ent, "orgId", carrier);
//         String uniId = SessionManager.getCurrentUserUni();
//         if(orgId == null || orgId.trim().isEmpty() || !orgId.equals(uniId)) {
//             c.addError("Id", "Invalid media id!!!");
//             return;
//         }
    }
    
    public void getMediaList() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String uniId = SessionManager.getCurrentUserUni();
        if(!SessionManager.getCurrentUserType().equals("SYSADMIN") && !SessionManager.getCurrentUserType().equals("ADMIN"))
            c.setValue("orgId", uniId);
    }
}

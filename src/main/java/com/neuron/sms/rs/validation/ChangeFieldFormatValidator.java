/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.validation;

import com.neuron.cm.Exception.QException;
import com.neuron.cm.annotation.DateFormat;
import com.neuron.cm.domain.Carrier;
import com.neuron.cm.domain.SessionManager;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author otahmadov
 */
public class ChangeFieldFormatValidator {
    
    public static void changeDateFormat(Class cl) throws QException, ParseException {
        Carrier c = SessionManager.sessionCarrier();
        
        for(String s: c.getKeys()) {
            for(Field f: cl.getDeclaredFields()) {
                if (f.isAnnotationPresent(DateFormat.class)) {
                    if(s.equals(f.getName())) {
                        String oldDate = c.getValue(s).toString();
                        if(oldDate != null && !oldDate.trim().isEmpty()) {
                            Date date = new SimpleDateFormat("dd/MM/yyyy").parse(oldDate);
                            String result = new SimpleDateFormat("dd-MMM-yy").format(date).toString();
                            SessionManager.sessionCarrier().setValue(s, result);
                        }
                    }
                }
            } 
        }
    }
    
    public static void changeDateFormatInTable(Class cl, String tableName) throws QException, ParseException {
        Carrier c = SessionManager.sessionCarrier();
        String[] columns = c.getTableColumnNames(tableName);
        int rowCount = c.getTableRowCount(tableName);
        for(String s: columns) {
            for(Field f: cl.getDeclaredFields()) {
                if (f.isAnnotationPresent(DateFormat.class)) {
                    if(s.equals(f.getName())) {
                        for(int i = 0; i < rowCount; i++) {
                            String oldDate = c.getValue(tableName, i, s).toString();
                            if(oldDate != null && !oldDate.trim().isEmpty()) {
                                Date date = new SimpleDateFormat("dd/MM/yyyy").parse(oldDate);
                                String result = new SimpleDateFormat("dd-MMM-yy").format(date).toString();
                                SessionManager.sessionCarrier().setValue(tableName, i, s, result);
                            }
                        }
                    }
                }
            } 
        }
    }
    
}

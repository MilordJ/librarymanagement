/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.validation;

import com.neuron.cm.domain.Carrier;
import com.neuron.cm.domain.EntityLibraryLibraryRequests;
import com.neuron.cm.domain.SessionManager;
import com.neuron.cm.util.EntityManager;
import com.neuron.cm.view.EntityLibraryV_Resources_v4;
import com.neuron.cm.view.EntitySecurityV_Users_v4;
import com.neuron.cm.enums.DictionaryCode;
import com.neuron.cm.enums.DictionaryType;
import com.neuron.cm.view.EntityLibraryV_LibraryRequests_v4;
import com.neuron.sms.rs.enums.RequiredConstants;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Lito
 */
public class RequestValidator {
    
    // These are for ADMINSTRATIVE STUFF
    Class domainLibraryRequests = EntityLibraryLibraryRequests.class;
    public void insertNewLibraryRequests() throws Exception {
        ChangeFieldFormatValidator.changeDateFormat(domainLibraryRequests);
        Carrier carrier = SessionManager.sessionCarrier();
        carrier.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertLibraryRequest());
        Date dateRequestStarted = new Date(SessionManager.sessionCarrier().getValue("requestStarted").toString());
        Date dateRequestLastDay = new Date(SessionManager.sessionCarrier().getValue("requestLastDay").toString());
        if(dateRequestStarted.after(dateRequestLastDay) || dateRequestStarted.equals(dateRequestLastDay)){
            carrier.addError("invalidDate", "Dates are incorrect!!!");
            return;
        }
        carrier.setValue("requestCode", DictionaryCode.OWNER);
        String uniId = SessionManager.getCurrentUserUni();
        carrier.setValue("orgId", uniId);
        String resourceId = carrier.getValue("resourceId").toString();
        String personId = carrier.getValue("personId").toString();
        EntityLibraryV_LibraryRequests_v4 requests_v4 = new EntityLibraryV_LibraryRequests_v4();
        Carrier c = new Carrier();
        c = new Carrier();
        c.setValue("resourceId", resourceId);
        c.setValue("personId", personId);
        c = EntityManager.getEntValuesByCarrier(requests_v4, c);
        int rowCount = c.getTableRowCount(requests_v4.toTableName());
        
        for(int i = 0; i < rowCount; i++) {
            String statusId = c.getValue(requests_v4.toTableName(), i, "status").toString();
            if(statusId.equals(DictionaryType.LIBRARY_REQUEST)) {
                c.addErrorMessage("repeatRequest", "Siz bu resurs üçün artıq müraciət etmisiniz. Zəhmət olmasa kitabxanaya yaxınlaşın!!!");
                return;
            } else if(statusId.equals(DictionaryType.LIBRARY_GIVEN_REQUEST)) {
                c.addErrorMessage("repeatRequest", "Bu resurs artıq sizdədir!!!");
                return;
            } else if(statusId.equals(DictionaryType.LIBRARY_LOADING_REQUEST)) {
                c.addErrorMessage("repeatRequest", "Müraciətiniz qeydə alınıb. Resurs mövcud olduqda sizə bildiriş göndəriləcəkdir!!!");
                return;
            }
        }
        
        c = new Carrier();
        c.setValue("id", resourceId);
        EntityLibraryV_Resources_v4 resources = new EntityLibraryV_Resources_v4();
        String count = EntityManager.getColumnValueByColumnName(resources, "restCount", c);
        if(count.equals("0")){
            carrier.addError("invalidResourceCount", resourceId);
            return;
        }
    }
    
    public void updateLibraryRequests() throws Exception {
        ChangeFieldFormatValidator.changeDateFormat(domainLibraryRequests);
        Carrier carrier = SessionManager.sessionCarrier();
        carrier.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getUpdateLibraryRequest());        
        Date dateRequestStarted = new Date(SessionManager.sessionCarrier().getValue("requestStarted").toString());
        Date dateRequestLastDay = new Date(SessionManager.sessionCarrier().getValue("requestLastDay").toString());
        if(dateRequestStarted.after(dateRequestLastDay) || dateRequestStarted.equals(dateRequestLastDay)){
            carrier.addError("invalidDate", "Dates are incorrect!!!");
            return;
        }
        carrier.setValue("requestCode", DictionaryCode.OWNER);
        String uniId = SessionManager.getCurrentUserUni();
        carrier.setValue("orgId", uniId);        
        String resourceId = carrier.getValue("resourceId").toString();
        String personId = carrier.getValue("personId").toString();
        EntityLibraryV_LibraryRequests_v4 requests_v4 = new EntityLibraryV_LibraryRequests_v4();
        Carrier c = new Carrier();
        c = new Carrier();
        c.setValue("resourceId", resourceId);
        c.setValue("personId", personId);
        c = EntityManager.getEntValuesByCarrier(requests_v4, c);
        int rowCount = c.getTableRowCount(requests_v4.toTableName());
        
        for(int i = 0; i < rowCount; i++) {
            String statusId = c.getValue(requests_v4.toTableName(), i, "status").toString();
            if(statusId.equals(DictionaryType.LIBRARY_REQUEST)) {
                c.addErrorMessage("repeatRequest", "Siz bu resurs üçün artıq müraciət etmisiniz. Zəhmət olmasa kitabxanaya yaxınlaşın!!!");
                return;
            } else if(statusId.equals(DictionaryType.LIBRARY_GIVEN_REQUEST)) {
                c.addErrorMessage("repeatRequest", "Bu resurs artıq sizdədir!!!");
                return;
            } else if(statusId.equals(DictionaryType.LIBRARY_LOADING_REQUEST)) {
                c.addErrorMessage("repeatRequest", "Müraciətiniz qeydə alınıb. Resurs mövcud olduqda sizə bildiriş göndəriləcəkdir!!!");
                return;
            }
        }
        
        c = new Carrier();

        c.setValue("id", resourceId);
        EntityLibraryV_Resources_v4 resources = new EntityLibraryV_Resources_v4();
        String count = EntityManager.getColumnValueByColumnName(resources, "restCount", c);
        if(count.equals("0")){
            carrier.addError("invalidResourceCount", resourceId);
            return;
        }
    }
    
    public void deleteLibraryRequests() throws Exception {
        Carrier carrier = SessionManager.sessionCarrier();
        carrier.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getDeleteLibraryRequest());
        String uniId = SessionManager.getCurrentUserUni();
        carrier.setValue("orgId", uniId);
    }
    
    public void getLibraryRequestsList() throws Exception {
        Carrier carrier = SessionManager.sessionCarrier();
        String uniId = SessionManager.getCurrentUserUni();
        carrier.setValue("orgId", uniId);
    }
    
    // These are for COMMUNITY
    
    public void insertNewLibraryRequestsCommon() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertLibraryRequestCommon());  
        
        String userId = SessionManager.getCurrentUserId();        
        EntitySecurityV_Users_v4 users_v4 = new EntitySecurityV_Users_v4();
        Carrier carrier = new Carrier();
        carrier.setValue("id", userId);
        String personId = EntityManager.getColumnValueByColumnName(users_v4, "personId", carrier);
        
        String resourceId = c.getValue("resourceId").toString();
        EntityLibraryV_LibraryRequests_v4 requests_v4 = new EntityLibraryV_LibraryRequests_v4();
        carrier = new Carrier();
        carrier.setValue("resourceId", resourceId);
        carrier.setValue("personId", personId);
        carrier = EntityManager.getEntValuesByCarrier(requests_v4, carrier);
        int rowCount = carrier.getTableRowCount(requests_v4.toTableName());
        
        for(int i = 0; i < rowCount; i++) {
            String statusId = carrier.getValue(requests_v4.toTableName(), i, "status").toString();
            if(statusId.equals(DictionaryType.LIBRARY_REQUEST)) {
                c.addErrorMessage("repeatRequest", "Siz bu resurs üçün artıq müraciət etmisiniz. Zəhmət olmasa kitabxanaya yaxınlaşın!!!");
                return;
            } else if(statusId.equals(DictionaryType.LIBRARY_GIVEN_REQUEST)) {
                c.addErrorMessage("repeatRequest", "Bu resurs artıq sizdədir!!!");
                return;
            } else if(statusId.equals(DictionaryType.LIBRARY_LOADING_REQUEST)) {
                c.addErrorMessage("repeatRequest", "Müraciətiniz qeydə alınıb. Resurs mövcud olduqda sizə bildiriş göndəriləcəkdir!!!");
                return;
            }
        }
        
        if(!c.hasError()) {
            carrier = new Carrier();
            carrier.setValue("personId", personId);

            if(SessionManager.getCurrentUserType().equals("STUDENT"))
                carrier.setValue("requestCode", DictionaryCode.STUDENT);
            else 
                carrier.setValue("requestCode", DictionaryCode.OWNER);

            carrier.setValue("resourceId", resourceId);
            carrier.setValue("status", DictionaryType.LIBRARY_REQUEST);
            String uniId = SessionManager.getCurrentUserUni();
            carrier.setValue("orgId", uniId);
            SessionManager.setSessionCarrier(carrier);
        }
        
    }
    
    public void updateLibraryRequestsCommon() throws Exception {// burda yanliz imtina ede biler
        Carrier c = SessionManager.sessionCarrier();
        String requestId = c.getValue("id").toString();        
        String resourceId = c.getValue("resourceId").toString();
        c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getUpdateLibraryRequestCommon());
        Carrier carrier = new Carrier();
        EntityLibraryV_LibraryRequests_v4 requests_v4 = new EntityLibraryV_LibraryRequests_v4();
        carrier.setValue("id", requestId);
        carrier.setValue("resourceId", resourceId);
        String status = EntityManager.getColumnValueByColumnName(requests_v4, "status", carrier);
        
        if(status.trim().isEmpty() || !status.equals(DictionaryType.LIBRARY_REQUEST)){
            c.addError("invalidParams", "Invalid parameters");
            c.addErrorMessage("invalidParams", "Müraciət mövcud deyil!!!");
            return;
        }
        
        carrier = new Carrier();
        carrier.setValue("id", requestId);
        carrier.setValue("status", DictionaryType.LIBRARY_CANCEL_REQUEST);
        SessionManager.setSessionCarrier(carrier);
    }
    
//    public void deleteLibraryRequestsCommon() throws Exception {
//        Carrier carrier = SessionManager.sessionCarrier();
//        carrier.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getDeleteLibraryRequestCommon());
//        String requestId = carrier.getValue("id").toString();
//        EntityLibraryV_LibraryRequests_v4 requests_v4 = new EntityLibraryV_LibraryRequests_v4();
//        Carrier c = new Carrier();
//        c.setValue("id", requestId);        
//        String status = EntityManager.getColumnValueByColumnName(requests_v4, "status", c);
//        if(!status.equals(DictionaryType.LIBRARY_REQUEST)){
//            c.addError("confirmedRequest", "Request already confirmed");
//            return;
//        }
//        String uniId = SessionManager.getCurrentUserUni();
//        carrier.setValue("orgId", uniId);
//    }
    
    public void getLibraryRequestsListCommon() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String userId = SessionManager.getCurrentUserId();
        String type = c.getValue("type").toString();// R requestler ucun olan koddur, G goturduyu kitablar, B qaytardigi kitablar, C imtina etdiyi kitablar, L gozlemede olan kitablar
        EntitySecurityV_Users_v4 users_v4 = new EntitySecurityV_Users_v4();
        Carrier carrier = new Carrier();
        carrier.setValue("id", userId);
        String personId = EntityManager.getColumnValueByColumnName(users_v4, "personId", carrier);
        c.setValue("personId", personId);
        String uniId = SessionManager.getCurrentUserUni();
        c.setValue("orgId", uniId);
        if(type.equals("R")) 
            c.setValue("status", DictionaryType.LIBRARY_REQUEST);
        else if(type.equals("G")) 
            c.setValue("status", DictionaryType.LIBRARY_GIVEN_REQUEST);
        else if(type.equals("B")) 
            c.setValue("status", DictionaryType.LIBRARY_BACK_REQUEST);
        else if(type.equals("L")) 
            c.setValue("status", DictionaryType.LIBRARY_LOADING_REQUEST);
        else if(type.equals("C")) 
            c.setValue("status", DictionaryType.LIBRARY_CANCEL_REQUEST);
        else
            c.addError("type", "invalid type");
    }
}

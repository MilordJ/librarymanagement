/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.validation;

import com.neuron.cm.domain.Carrier;
import com.neuron.cm.domain.SessionManager;
import com.neuron.cm.util.EntityManager;
import com.neuron.cm.view.EntityLibraryV_Authors_v4;
import com.neuron.sms.rs.enums.RequiredConstants;

/**
 *
 * @author Lito
 */
public class AuthorValidator {

    public void insertNewAuthor() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertAuthor());
        String uniId = SessionManager.getCurrentUserUni();
//        if (!SessionManager.getCurrentUserType().equals("SYSADMIN") && !SessionManager.getCurrentUserType().equals("ADMIN")) {
            c.setValue("orgId", uniId);
//        }
        if (SessionManager.getCurrentUserType().equals("STUDENT") && !SessionManager.getCurrentUserType().equals("TEACHER")) {
            c.addError("userType", "Invalid user type");
        }
    }

    public void updateAuthor() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getUpdateAuthor());
        String id = c.getValue("id").toString();
        EntityLibraryV_Authors_v4 ent = new EntityLibraryV_Authors_v4();
        Carrier carrier = new Carrier();
        carrier.setValue("id", id);
        String orgId = EntityManager.getColumnValueByColumnName(ent, "orgId", carrier);
        String uniId = SessionManager.getCurrentUserUni();
        if (orgId == null || orgId.trim().isEmpty() || !orgId.equals(uniId)) {
            c.addError("Id", "Invalid author id!!!");
            return;
        }
    }

    public void deleteAuthor() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getDeleteAuthor());
        String id = c.getValue("id").toString();
        EntityLibraryV_Authors_v4 ent = new EntityLibraryV_Authors_v4();
        Carrier carrier = new Carrier();
        carrier.setValue("id", id);
        String orgId = EntityManager.getColumnValueByColumnName(ent, "orgId", carrier);
        String uniId = SessionManager.getCurrentUserUni();
        if (orgId == null || orgId.trim().isEmpty() || !orgId.equals(uniId)) {
            c.addError("Id", "Invalid author id!!!");
            return;
        }
    }

    public void getAuthorList() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String uniId = SessionManager.getCurrentUserUni();
        if (!SessionManager.getCurrentUserType().equals("SYSADMIN") && !SessionManager.getCurrentUserType().equals("ADMIN")) {
            c.setValue("orgId", uniId);
        }
    }
}

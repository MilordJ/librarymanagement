/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.enums;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 *
 * @author otahmadov
 */
@Component
@PropertySource("classpath:required.properties")
@ConfigurationProperties
@Data
public class RequiredConstants {
    private String insertResources;
    private String updateResources;
    private String deleteResources;
    private String insertResourcesEdition;
    private String insertResourcesAuthor;
    private String updateResourcesEdition;
    private String deleteResourcesEdition;
    private String insertResourcesCompiler;
    private String updateResourcesCompiler;
    private String deleteResourcesCompiler;
    private String insertAuthor;
    private String updateAuthor;
    private String deleteAuthor;
    private String insertMedia;
    private String updateMedia;
    private String deleteMedia;
    private String insertScientificResearch;
    private String updateScientificResearch;
    private String deleteScientificResearch;
    private String insertLibraryRequest;
    private String updateLibraryRequest;
    private String deleteLibraryRequest;
    private String insertLibraryRequestCommon;
    private String updateLibraryRequestCommon;
    private String deleteLibraryRequestCommon;
    
}

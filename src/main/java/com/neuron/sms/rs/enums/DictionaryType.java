/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.enums;

/**
 *
 * @author otahmadov
 */
public class DictionaryType {
    public static final String MILITARY_DOCUMENT = "1000119";
    public static final String HEALTH_DOCUMENT = "1000118";
    public static final String SOCIAL_STATUS_DOCUMENT = "1000117";
    public static final String ORPHAN_DEGREE_DOCUMENT = "1000132";
    public static final String PERSONAL_DOCUMENT = "1000029";
    public static final String SCHOOL_DOCUMENT = "1000042";
    public static final String WORK_DOCUMENT = "1000041";
    public static final String STUDENT_DOCUMENT = "1000040";
}
